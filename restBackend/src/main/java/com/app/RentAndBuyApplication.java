package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentAndBuyApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentAndBuyApplication.class, args);
	}

}
